﻿using System;
using NUnit.Framework;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzTest
    {
        private NumberHelper _numberHelper;

        [SetUp]
        public void Init()
        {
            _numberHelper = new NumberHelper();
        }


        [Test]
        [TestCase(0)]
        [TestCase(-34)]
        [TestCase(-12323)]
        public void WhenTheNumberIsNotPostive_ShoulReturnEmpty(int number)
        {
            // Arrange
            var expected = String.Empty;

            // Act
            var actual = _numberHelper.GetFizzBuzz(number);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(0)]
        [TestCase(4000)]
        public void WhenTheNumberIsNotBetween1and1000_ShoulReturnEmpty(int number)
        {
            // Arrange
            var expected = String.Empty;

            // Act
            var actual = _numberHelper.GetFizzBuzz(number);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(2)]
        [TestCase(8)]
        [TestCase(11)]
        [TestCase(332)]
        public void WhenTheNumberIsNotDivisibleBy3And5_ShoulPrintNumber(int number)
        {
            // Arrange
            var expected = number.ToString();

            // Act
            var actual = _numberHelper.GetFizzBuzz(number);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(3)]
        [TestCase(9)]
        [TestCase(12)]
        [TestCase(333)]
        public void WhenTheNumberIsDivisibleBy3_ShoulPrintFizz(int number)
        {
            // Arrange
            const string expected = "fizz";

            // Act
            var actual =_numberHelper.GetFizzBuzz(number);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(5)]
        [TestCase(10)]
        public void WhenTheNumberIsDivisibleBy5_ShoulPrintBuzz(int number)
        {
            // Arrange
            const string expected = "buzz";

            // Act
            var actual = _numberHelper.GetFizzBuzz(number);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(15)]
        [TestCase(300)]
        public void WhenTheNumberIsDivisibleBy3And5_ShoulPrintFizzBuzz(int number)
        {
            // Arrange
            const string expected = "fizz buzz";

            // Act
            var actual = _numberHelper.GetFizzBuzz(number);

            // Assert
            Assert.That(actual, Is.EqualTo(expected));
        }
        
    }
}
