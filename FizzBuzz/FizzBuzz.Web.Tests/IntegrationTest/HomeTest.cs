﻿
using System;
using System.Web.Mvc;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Web.Tests.IntegrationTest
{
    [TestFixture]
    public class HomeTest
    {
        private Mock<INumberHelper> _numberHelperMock;
        HomeController _controller;

        [SetUp]
        public void Init()
        {
            // Arrange
            _numberHelperMock = new Mock<INumberHelper>();
            _numberHelperMock.Setup(x => x.GetFizzBuzz(It.IsAny<int>())).Returns("Test Data");
            _controller = new HomeController(_numberHelperMock.Object);

        }

        [Test]
        public void Index()
        {
            // Act
            ViewResult result = _controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result, "Not a viewResult");
        }

        [Test]
        public void GetFizzBuzz_WithEmptyData_ReturnIndexViewWithError()
        {
            // Act
            var result = _controller.GetFizzBuzz(null) as ViewResult;

            // Assert
            Assert.IsNotNull(result, "Not a viewResult");
            Assert.IsNull(result.Model, "Empty a model");
        }

        [Test]
        public void GetFizzBuzz_WithModel_ReturnIndexViewWithError()
        {
            // Arrange
            var viewModel = new HomeViewModel();
            viewModel.Number = 100;

            // Act
            var result = _controller.GetFizzBuzz(viewModel) as ViewResult;

            // Assert
            Assert.IsNotNull(result, "Not a viewResult");
            Assert.IsTrue(result.TempData.ContainsKey("FizzBuzz"), "Empty a tempdata");
        }
    }
}
