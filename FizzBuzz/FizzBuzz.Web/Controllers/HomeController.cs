﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Web.Models;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly INumberHelper _numberHelper;
        public HomeController(INumberHelper numberHelper)
        {
            _numberHelper = numberHelper;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetFizzBuzz(HomeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index");
            }
            // model
            FizzBuzzModel fizzBuzzModel = new FizzBuzzModel();
            fizzBuzzModel.Prints = new List<string>();
            for (int i = 1; i < model.Number; i++)
            {
                fizzBuzzModel.Prints.Add(_numberHelper.GetFizzBuzz(i));
            }
            TempData.Add("FizzBuzz", fizzBuzzModel);

            return View("Index");
        }
    }
}