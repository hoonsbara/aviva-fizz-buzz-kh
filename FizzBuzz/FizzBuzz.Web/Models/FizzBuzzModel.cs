﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzModel
    {
        public List<string> Prints { get; set; }
    }
}