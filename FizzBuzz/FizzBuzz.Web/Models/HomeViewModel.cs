﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class HomeViewModel
    {
        [Required]
        [Display(Name = "Input Number")]
        [Range(1, 1000, ErrorMessage = "You should input a number between 1 and 1000.")]
        public int Number { get; set; }
    }
}