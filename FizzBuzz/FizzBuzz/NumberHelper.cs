﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class NumberHelper : INumberHelper
    {
        public string GetFizzBuzz(int number)
        {
            if (number < 1 || number >= 1000)
                return "";

            if(number % 3 == 0 && number % 5 == 0)
                return "fizz buzz";
            else if (number % 3 == 0)
                return "fizz";
            else if (number % 5 == 0)
                return "buzz";

            return number.ToString();
        }
    }
}
